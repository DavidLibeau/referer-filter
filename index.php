<?php

if(isset($_GET["debug"])){
    error_reporting( E_ALL );

    echo("Your referer is : <br/>");
    echo("&gt;".$_SERVER["HTTP_REFERER"]."&lt;<br/><br/>");
}

$blocked=array("/^http(s)?:\/\/t\.co\//i","/^http(s)?:\/\/github\.com\//i","/^http(s)?:\/\/blog\.davidlibeau\.fr\//i");
$blockedbool=false;

foreach($blocked as $b){
    if(preg_match($b,$_SERVER["HTTP_REFERER"])){
        if(isset($_GET["debug"])){
            echo("Blocked : ".$b."<br/>");
            $blockedbool=true;
        }else{
            header("Location: ".$_SERVER["HTTP_REFERER"]);
            die();
        }
    }
}

if(!$blockedbool){
    echo("This is the normal content. You have not been blocked.");
}